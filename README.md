Semana 8 omnistack da Rocketseat!!
Bora codar!

Demonstrações:

- Web ![ALT](/Demonstracao_web.gif)

- Mobile(Android) ![ALT](/Demonstracao_android.gif)

Pré-Requisitos para rodar aplicação:
- Instalar yarn
- Jdk8, Python2, React, React-Native, Node-Js

Passos para rodar a aplicação:

1º abrir a pasta /backend no Visual Code e executar o comando yarn dev
2º abrir a pasta /frontened no Visual Code e executar o comando yarn start
3º abrir a pasta /tindev no Visual Code e executar o comando reat-native run-android